# Kubernetes Deployment

This repository stores a couple configuration files and documents the process to deploy the project to a Kubernetes cluster.

# Prerequisites

* A Kubernetes cluster (tested on a v1.19.5+k3s2)
  * Traefik was used as Ingress
* Access to the other projects of this group (5DATA-PARIS-2122)
* A correct DNS configuration (out of the scope of this project)

# Quick-start

* Run `kubectl apply -f prep-5data.yml`
* Get the newly created role's secret token:
```
SECRET=$(kubectl -n school-5data get secret | grep gitlab | awk '{print $1}')
TOKEN=$(kubectl -n school-5data get secret $SECRET -o jsonpath='{.data.token}' | base64 --decode)
echo $TOKEN
```
* Get the cluster's CA: 
```
kubectl config view --raw \
-o=jsonpath='{.clusters[0].cluster.certificate-authority-data}' \
| base64 --decode
```
* Go to the Gitlab group's CI/CD settings and create the following Variables:
  * KUBE_CA: self-explanatory, **set it as a File type var**
  * KUBE_CLUSTER: master node's URI, i.e. `https://master.kube.tld:6443`
  * PROJECT_DEPLOY_TOKEN: the token stored in $TOKEN earlier
* Then, in each projects that need to be deployed, create the following CI/CD variables:
  * PRODUCTION_URL: the main URL i.e. `www.domain.tld`
  * STAGING_URL: the URL used for staging envs i.e. `staging.www.domain.tld` (a branch named `test` would be deployed to `test.staging.www.domain.tld`)